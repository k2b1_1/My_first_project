﻿import matplotlib.pyplot as plt


x = []
y = []
c = 0
with open("res.txt", "r") as f:
    for line in f:
        #print(line)
        xx = int(line.replace('A('+str(c)+') = [', '').replace(']\n', '').split(', ')[0])
        yy = int(line.replace('A('+str(c)+') = [', '').replace(']\n', '').split(', ')[1])
        x.append(xx)
        y.append(yy)
        c += 1

plt.plot(x,y,'r')
plt.show()