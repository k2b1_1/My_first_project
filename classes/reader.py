class ReadFile:
    
    def __init__(self, path):
        self.path = path

    def readfile(self):
        print("""СЧИТЫВАНИЕ КООРДИНАТ ОТРЕЗКА
        """)
        a = []
        x = []
        y = []
        c = 0
        with open(self.path, "r") as f:
            for line in f:
                #print(line)
                xx = int(line.replace('A('+str(c)+') = [', '').replace(']\n', '').split(', ')[0])
                yy = int(line.replace('A('+str(c)+') = [', '').replace(']\n', '').split(', ')[1])
                a.append([xx, yy])
                x.append(xx)
                y.append(yy)
                c += 1
        return x, y

