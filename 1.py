﻿import random

print("""ГЕНЕРАЦИЯ КООРДИНАТ ОТРЕЗКА
""")
c = 1

x = [random.randint(0, 20) for i in range(10)]
y = [random.randint(0, 20) for i in range(10)]

a = [0]*10

for i in range(10):
    a[i] = [x[i], y[i]]
print(a)

with open("res.txt", "w") as f:
    for i in range(10):
        f.write('A('+str(i)+') = '+str(a[i])+'\n')
print('координаты сохранены в файл')
